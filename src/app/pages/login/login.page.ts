import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, ReactiveFormsModule]
})
export class LoginPage implements OnInit {
  form: FormGroup = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]]
  });
  constructor(private authService: AuthService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
  }
  login(){
    if(this.form.valid){
      const {email, password} = this.form.getRawValue();
      this.authService.login(email, password)
      .then( (user) => {
        this.router.navigate(['home']);
      }
      )
      .catch( error => {
        console.log(error);
      }

      )
    }else{
      this.form.markAllAsTouched();
    }
  }
  register(){
    this.router.navigate(['register']);
  }

}
