import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { filter } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule]
})
export class HomePage implements OnInit {
  user$ = this.auth.authState$.pipe(
    filter(state => state ? true : false)
  );
  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
  }
   
  async logout(){
    await this.auth.logout();
    this.router.navigate(['login']);
  }

}
